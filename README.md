# Spring Data JPA CRUD Example - Spring Boot Tutorials for Beginners
# Following Technologies Used
- Eclipse IDE and Spring Tool Suite plugin for Eclipse
- Spring Boot 2.1.0.BUILD-SNAPSHOT
- spring-boot-starter-data-jpa
- spring-boot-starter-web
- mysql-connector-java
- JSTL - JSP Standard Tag Library
- tomcat-embed-jasper
- Bootstrap 4.0.0 Webjars
- Java 8+

# Creating database and tables in MySQL
````sql
CREATE DATABASE `jackrutorial` /*!40100 DEFAULT CHARACTER SET utf8 */;

DROP TABLE IF EXISTS `jackrutorial`.`article`;
CREATE TABLE  `jackrutorial`.`article` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(100) NOT NULL default '',
  `category` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
````
* We will add the required dependencies to Maven pom.xml file 

````java
<dependency>
 <groupId>javax.servlet</groupId>
 <artifactId>jstl</artifactId>
</dependency>
<dependency>
 <groupId>org.apache.tomcat.embed</groupId>
 <artifactId>tomcat-embed-jasper</artifactId>
 <scope>provided</scope>
</dependency>
<dependency>
 <groupId>org.webjars</groupId>
 <artifactId>bootstrap</artifactId>
 <version>4.0.0</version>
</dependency>
````
