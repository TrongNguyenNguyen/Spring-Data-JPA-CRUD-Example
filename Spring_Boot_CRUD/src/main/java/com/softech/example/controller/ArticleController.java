package com.softech.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.softech.example.model.Article;
import com.softech.example.service.ArticleService;

@Controller
@RequestMapping(value = "/article")
public class ArticleController {

	@Autowired
	ArticleService articleService;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView modelAndView = new ModelAndView("article_list");
		List<Article> articles = articleService.getAllService();
		modelAndView.addObject("articleList", articles);
		return modelAndView;
	}

	@RequestMapping(value = "/addArticle/", method = RequestMethod.GET)
	public ModelAndView addArticle() {
		ModelAndView modelAndView = new ModelAndView();
		Article article = new Article();
		modelAndView.addObject("articleForm", article);
		modelAndView.setViewName("article_form");
		return modelAndView;
	}

	@RequestMapping(value = "/updateArticle/{id}", method = RequestMethod.GET)
	public ModelAndView editArticle(@PathVariable long id) {
		ModelAndView modelAndView = new ModelAndView();

		Article article = articleService.getArticleById(id);
		modelAndView.addObject("articleForm", article);
		modelAndView.setViewName("article_form");
		return modelAndView;
	}

	@RequestMapping(value = "/saveArticle", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("articleForm") Article article) {
		articleService.saveOrUpdate(article);
		return new ModelAndView("redirect:/article/list");
	}

	@RequestMapping(value = "/deleteArticle/{id}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable("id") long id) {
		articleService.deleteArticle(id);
		return new ModelAndView("redirect:/article/list");
	}

}
