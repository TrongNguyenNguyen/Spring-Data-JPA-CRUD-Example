package com.softech.example.service;

import java.util.List;

import com.softech.example.model.*;

public interface ArticleService {

	public List<Article> getAllService();
	
	public Article getArticleById(long id);
	
	public void saveOrUpdate(Article article);
	
	public void deleteArticle(long id);
	
}
