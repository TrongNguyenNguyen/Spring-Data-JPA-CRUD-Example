package com.softech.example.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softech.example.model.Article;
import com.softech.example.repository.ArticleRepository;

@Service
@Transactional
public class ArticleServiceImpl implements ArticleService {

	@Autowired
	ArticleRepository articleRepository;

	@Override
	public List<Article> getAllService() {
		return (List<Article>) articleRepository.findAll();
	}

	@Override
	public Article getArticleById(long id) {
		return articleRepository.findById(id).get();
	}

	@Override
	public void saveOrUpdate(Article article) {
		articleRepository.save(article);
	}

	@Override
	public void deleteArticle(long id) {
		articleRepository.deleteById(id);
	}

}
