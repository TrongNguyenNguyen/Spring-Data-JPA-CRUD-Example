package com.softech.example.repository;

import org.springframework.data.repository.CrudRepository;

import com.softech.example.model.Article;

public interface ArticleRepository extends CrudRepository<Article, Long> {

}
